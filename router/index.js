import  express  from 'express';
import  json  from 'body-parser';
import { render } from 'ejs';


export const router = express.Router();

router.get('/',(req,res)=>{

    res.render('index',{titulo:"Compañia de luz", nombre   :"Juan Rivera"})

})

router.post('/recibo',(req,res)=>{

    const params = {
        numero:req.body.numero,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        servicio:req.body.servicio,
        kilow:req.body.kilow
    }

    res.render('recibo',params);
})

router.get('/recibo',(req,res)=>{

    const params = {
        numero:req.query.numero,
        nombre:req.query.nombre,
        domicilio:req.query.domicilio,
        servicio:req.query.servicio,
        kilow:req.query.kilow
    }

    res.render('recibo',params);
})


export default {router};